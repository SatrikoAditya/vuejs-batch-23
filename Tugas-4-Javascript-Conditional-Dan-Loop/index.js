// Soal 1
var nilai = 84;

if(nilai >= 85) {
  console.log('A')
} else if(nilai >= 75) {
  console.log('B') 
} else if(nilai >= 65) {
  console.log('C')
} else if(nilai >= 55) {
  console.log('D')
} else {
  console.log('E')
}

// Soal 2
var tanggal = 22;
var bulan = 11;
var tahun = 2020;

switch(bulan) {
  case 1:
    console.log(tanggal + ' Januari ' + tahun)
    break;
  case 2:
    console.log(tanggal + ' Februari ' + tahun)
    break;
  case 3:
    console.log(tanggal + ' Maret ' + tahun)
    break;
  case 4:
    console.log(tanggal + ' April ' + tahun)  
    break;
  case 5:
    console.log(tanggal + ' Mei ' + tahun)
    break;
  case 6:
    console.log(tanggal + ' Juni ' + tahun)
    break;
  case 7:
    console.log(tanggal + ' Juli ' + tahun)
    break;
  case 8:
    console.log(tanggal + ' Agustus ' + tahun) 
    break;
  case 9:
    console.log(tanggal + ' September ' + tahun)
    break;
  case 10:
    console.log(tanggal + ' Oktober ' + tahun)
    break;
  case 11:
    console.log(tanggal + ' November ' + tahun)
    break;
  case 12:
    console.log(tanggal + ' Desember ' + tahun)
    break;
  default:
    console.log(tanggal + ' ' + bulan + ' ' + tahun)
    break;
}

// Soal 3
var n = 7;
var s = '';

for(var i = 1; i <= n; i++) {
  for(var j = 1; j <= i; j++) {
    s+= '#'
  }
  s += '\n'
}
console.log(s)

// Soal 4
var m = 10
var line = '=';

for(var k = 1; k <= m; k++) {
  if(k % 3 === 1) {
    console.log(k + ' - I love programming')
  } else if(k % 3 === 2) {
    console.log(k + ' - I love Javascript')
  } else if(k % 3 === 0) {
    console.log(k + ' - I love VueJS')
    console.log(line)
  }
  line+= '='
}