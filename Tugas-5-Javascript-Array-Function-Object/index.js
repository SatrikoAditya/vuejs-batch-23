// Soal 1
var daftarBuah = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarBuah.sort((a, b) => Number(a[0]) - Number(b[0]))
for(var i = 0; i < daftarBuah.length; i++) {
  console.log(daftarBuah[i])
}

// Soal 2
function introduce(data) {
  return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}`
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan)

// Soal 3
function hitung_huruf_vokal(string) {
  var vocal = 'aiueoAIUEO'
  var totalVocal = 0
  for(var j = 0; j < string.length; j++) {
    for(var k = 0; k < vocal.length; k++) {
      if(string[j] === vocal[k]) {
        totalVocal++
      }
    }
  }
  return totalVocal
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

//Soal 4
function hitung(angka) {
  return angka * 2 - 2
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8